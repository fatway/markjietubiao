﻿/*
 * 由SharpDevelop创建。
 * 用户： Lee
 * 日期: 2012/6/1
 * 时间: 11:00
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Diagnostics;
using Excel = Microsoft.Office.Interop.Excel;


namespace MarkJietubiao
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}
		
		//选择由JietubiaoCreater生成的接图表CSV文件
		void BtnGridCSVClick(object sender, EventArgs e)
		{
			OpenFileDialog grid = new OpenFileDialog();
			grid.Title = "选择由JietubiaoCreater生成的接图表CSV文件";
			grid.Filter = "*.csv|*.csv";
			
			if(grid.ShowDialog() == DialogResult.OK)
				txtGrid.Text = grid.FileName;
		}
		
		//指定计划部已选择可以打图的txt列表文件
		void BtnSelectListClick(object sender, EventArgs e)
		{
			OpenFileDialog list = new OpenFileDialog();
			list.Title = "指定计划部已选择可以打图的txt列表文件";
			list.Filter = "*.txt|*.txt";
			
			if(list.ShowDialog() == DialogResult.OK)
				txtList.Text = list.FileName;
		}
		
		
		//指定保存Excel表格的位置和名称
		void BtnOutExcelClick(object sender, EventArgs e)
		{
			SaveFileDialog excelfile = new SaveFileDialog();
			excelfile.Title = "指定保存Excel表格的位置和名称";
			excelfile.Filter = "*.xls|*.xls";
			
			if(excelfile.ShowDialog() == DialogResult.OK)
				txtExcel.Text = excelfile.FileName;
		}
		
		void BtnCreateClick(object sender, EventArgs e)
		{
			if(txtGrid.Text.Length == 0 || txtList.Text.Length == 0 || txtExcel.Text.Length == 0)
				return;
			
			//读取csv接图表网格数据
			List<string[]> grid_data = new List<string[]>();
			StreamReader csvreader = new StreamReader(txtGrid.Text, Encoding.Default);
			string strLine = "";
			while((strLine = csvreader.ReadLine()) != null)
			{
				grid_data.Add(strLine.ToString().Split(','));
			}
			
			//更新方法，改用字典来实现唯一值   //读取选择图幅到列表中
			//List<string> list_data = new List<string>();
            Dictionary<string, int> list_data = new Dictionary<string, int>();
			StreamReader txtreader = new StreamReader(txtList.Text, Encoding.Default);
			while((strLine = txtreader.ReadLine()) != null)
			{
                try
                {
                    list_data.Add(strLine, 0);
                }
                catch
                {
                    MessageBox.Show("已选图幅中存在重复值，无法继续", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
			}
			
			
			//输出excel表格并输出接图表，已选择的将底色标记为黄色
			//
			string xltpath = txtExcel.Text;
			object missing = System.Reflection.Missing.Value;
			
			//创建表格对象
            Excel.Application app = new Excel.Application();
            app.Application.Workbooks.Add(true);
            Excel.Workbook book = (Excel.Workbook)app.ActiveWorkbook;
            Excel.Worksheet sheet = (Excel.Worksheet)book.ActiveSheet;

            //接网格输出内容
            for (int row = 0; row < grid_data.Count; row++)
            {
            	int col_num = grid_data[row].Length;  //获取列数
            	for (int col = 0; col < col_num; col++)
            	{
            		sheet.Cells[row+1, col+1] = grid_data[row][col];
            		
                    ////标记黄色
                    //if(grid_data[row][col] in list_data)
                    //{
                    //    //range _Worksheet.get_Rang("A3","A3").Interior.Color = Color.Yellow;
                    //    Excel.Range rng = (Excel.Range)sheet.Cells[row+1, col+1];
                    //    rng.Interior.Color = Color.Yellow;
                    //}
                    try
                    {
                        list_data.Add(grid_data[row][col], 0);
                    }
                    catch
                    {
                        Excel.Range rng = (Excel.Range)sheet.Cells[row + 1, col + 1];
                        rng.Interior.Color = 49407;  //5 6 黑色；
                    }
            	}
            }

            //保存
     		//book.SaveCopyAs(txtExcel.Text); //由于调用的是2007的模块，默认保存为2007的格式，打开后会提示格式不一致，保存时指定用旧格式即可解决问题。
            book.SaveAs(txtExcel.Text, Excel.XlFileFormat.xlExcel7, missing, missing, missing, missing, Excel.XlSaveAsAccessMode.xlNoChange, missing, missing, missing, missing, missing);
        	//关闭文件
            book.Close(false, missing, missing);
            //退出excel
            app.Quit();

            MessageBox.Show("表格生成并标记完成！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		
	}
}
