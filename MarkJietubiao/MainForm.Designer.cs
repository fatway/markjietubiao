﻿/*
 * 由SharpDevelop创建。
 * 用户： Lee
 * 日期: 2012/6/1
 * 时间: 11:00
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace MarkJietubiao
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnGridCSV = new System.Windows.Forms.Button();
            this.btnSelectList = new System.Windows.Forms.Button();
            this.btnOutExcel = new System.Windows.Forms.Button();
            this.txtGrid = new System.Windows.Forms.TextBox();
            this.txtList = new System.Windows.Forms.TextBox();
            this.txtExcel = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGridCSV
            // 
            this.btnGridCSV.Location = new System.Drawing.Point(76, 63);
            this.btnGridCSV.Name = "btnGridCSV";
            this.btnGridCSV.Size = new System.Drawing.Size(99, 23);
            this.btnGridCSV.TabIndex = 0;
            this.btnGridCSV.Text = "指定接图表CSV";
            this.btnGridCSV.UseVisualStyleBackColor = true;
            this.btnGridCSV.Click += new System.EventHandler(this.BtnGridCSVClick);
            // 
            // btnSelectList
            // 
            this.btnSelectList.Location = new System.Drawing.Point(76, 110);
            this.btnSelectList.Name = "btnSelectList";
            this.btnSelectList.Size = new System.Drawing.Size(99, 23);
            this.btnSelectList.TabIndex = 1;
            this.btnSelectList.Text = "选择出图的列表";
            this.btnSelectList.UseVisualStyleBackColor = true;
            this.btnSelectList.Click += new System.EventHandler(this.BtnSelectListClick);
            // 
            // btnOutExcel
            // 
            this.btnOutExcel.Location = new System.Drawing.Point(76, 160);
            this.btnOutExcel.Name = "btnOutExcel";
            this.btnOutExcel.Size = new System.Drawing.Size(99, 23);
            this.btnOutExcel.TabIndex = 2;
            this.btnOutExcel.Text = "指定输出表格";
            this.btnOutExcel.UseVisualStyleBackColor = true;
            this.btnOutExcel.Click += new System.EventHandler(this.BtnOutExcelClick);
            // 
            // txtGrid
            // 
            this.txtGrid.Location = new System.Drawing.Point(200, 65);
            this.txtGrid.Name = "txtGrid";
            this.txtGrid.Size = new System.Drawing.Size(320, 21);
            this.txtGrid.TabIndex = 3;
            // 
            // txtList
            // 
            this.txtList.Location = new System.Drawing.Point(200, 112);
            this.txtList.Name = "txtList";
            this.txtList.Size = new System.Drawing.Size(320, 21);
            this.txtList.TabIndex = 4;
            // 
            // txtExcel
            // 
            this.txtExcel.Location = new System.Drawing.Point(200, 162);
            this.txtExcel.Name = "txtExcel";
            this.txtExcel.Size = new System.Drawing.Size(320, 21);
            this.txtExcel.TabIndex = 5;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(413, 240);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(107, 39);
            this.btnCreate.TabIndex = 6;
            this.btnCreate.Text = "生成标记表格";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.BtnCreateClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 345);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.txtExcel);
            this.Controls.Add(this.txtList);
            this.Controls.Add(this.txtGrid);
            this.Controls.Add(this.btnOutExcel);
            this.Controls.Add(this.btnSelectList);
            this.Controls.Add(this.btnGridCSV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "华业龙图接图表标识器 - 计划部出图标记专用";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private System.Windows.Forms.Button btnCreate;
		private System.Windows.Forms.TextBox txtExcel;
		private System.Windows.Forms.TextBox txtList;
		private System.Windows.Forms.TextBox txtGrid;
		private System.Windows.Forms.Button btnOutExcel;
		private System.Windows.Forms.Button btnSelectList;
		private System.Windows.Forms.Button btnGridCSV;
	}
}
